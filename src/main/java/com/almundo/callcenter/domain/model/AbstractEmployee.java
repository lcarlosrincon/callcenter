package com.almundo.callcenter.domain.model;

public abstract class AbstractEmployee implements Employee {

	public interface Attributes {
		String NAME = "name";
		String BUSY = "busy";
	}

	private String name;
	private boolean busy;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isBusy() {
		return busy;
	}

	public void setBusy(boolean busy) {
		this.busy = busy;
	}

	public int compareTo(Employee o) {
		int compare = Integer.compare(getAttentionPriority(),
				o.getAttentionPriority());
		if (compare == 0
				&& AbstractEmployee.class.isAssignableFrom(o.getClass())
				&& name != null) {
			compare = name.compareTo(((AbstractEmployee) o).name);
		}
		return compare;
	}

	@Override
	public String toString() {
		return " [name=" + name + "]";
	}
}
