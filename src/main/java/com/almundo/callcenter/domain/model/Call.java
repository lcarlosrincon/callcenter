package com.almundo.callcenter.domain.model;


public class Call {

	public static final int CALL_MIN = 5;
	public static final int CALL_MAX = 10;

	private Integer number;

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return "Call [number=" + number + "]";
	}

}
