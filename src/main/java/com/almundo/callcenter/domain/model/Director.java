package com.almundo.callcenter.domain.model;

import com.almundo.callcenter.utils.PriorityAttention;

public class Director extends AbstractEmployee {

	public int getAttentionPriority() {
		return PriorityAttention.BAJA.getLevel();
	}

}
