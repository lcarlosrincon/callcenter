package com.almundo.callcenter.domain.model;

public interface Employee extends Comparable<Employee> {

	public int getAttentionPriority();

	public void setBusy(boolean busy);

	public boolean isBusy();

}
