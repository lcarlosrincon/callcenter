package com.almundo.callcenter.domain.model;

import com.almundo.callcenter.utils.PriorityAttention;

public class Operator extends AbstractEmployee {

	public int getAttentionPriority() {
		return PriorityAttention.ALTA.getLevel();
	}

}
