package com.almundo.callcenter.domain.model;

import com.almundo.callcenter.utils.PriorityAttention;

public class Supervisor extends AbstractEmployee {

	public int getAttentionPriority() {
		return PriorityAttention.MEDIA.getLevel();
	}

}
