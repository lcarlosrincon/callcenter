package com.almundo.callcenter.service;

import com.almundo.callcenter.domain.model.Call;
import com.almundo.callcenter.domain.model.Employee;

public interface CallCenterService {

	public Employee nextFree();

	public void converse(Call call, Employee employee);

	public void addEmployee(Employee employee);

}
