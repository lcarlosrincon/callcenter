package com.almundo.callcenter.service;

import com.almundo.callcenter.domain.model.Call;

public interface Dispatcher {

	public void dispatcherCall(Call callNew);

}
