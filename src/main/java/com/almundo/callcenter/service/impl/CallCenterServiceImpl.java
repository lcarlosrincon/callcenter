package com.almundo.callcenter.service.impl;

import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.almundo.callcenter.domain.model.Call;
import com.almundo.callcenter.domain.model.Employee;
import com.almundo.callcenter.service.CallCenterService;
import com.almundo.callcenter.utils.Constant;

/**
 * Servicio principal que representa la operacion del callCenter.
 * 
 * @author lrincon
 * 
 */
public class CallCenterServiceImpl implements CallCenterService {

	private static Logger LOGGER = Logger.getLogger(Call.class.getSimpleName());

	private Set<Employee> employees = new TreeSet<Employee>();

	/**
	 * Obtiene el empleado de acuerdo a la collection ordenada por prioridades
	 * de atencion, segun el tipo de empleado. Si no hay disponible, se suspende
	 * el hilo de ejecucion hasta que sea liberado uno de ellos.
	 * 
	 */
	public synchronized Employee nextFree() {
		for (Employee employee : getEmployees()) {
			if (!employee.isBusy()) {
				employee.setBusy(true);
				LOGGER.log(Level.INFO, "Find " + employee);
				return employee;
			}
		}
		return null;
	}

	/**
	 * Simula la conversacion de la call con el empleado. Duerme el hilo por
	 * unos segundos y una vez "finaliza" la llamada notifica respecto a la
	 * liberacion del empleado para que pueda operar otra llamada.
	 */
	public void converse(Call call, Employee employee) {
		LOGGER.log(Level.INFO, "[Call:" + call + ", employee:" + employee
				+ "] - start");
		int seconds = Call.CALL_MIN
				+ new Random().nextInt(Call.CALL_MAX - Call.CALL_MIN + 1);
		try {
			LOGGER.log(Level.INFO, "[Call:" + call + ", employee:" + employee
					+ "] - wait " + seconds + " seconds");
			Thread.sleep(seconds * Constant.SECONDS_TO_MILLISECONDS);
		} catch (InterruptedException e) {
			// finish time
		} finally {
			finish(call, employee);
		}
		LOGGER.log(Level.INFO, "[Call:" + call + ", employee:" + employee
				+ "]- finish");
	}

	public synchronized void finish(Call call, Employee employee) {
		employee.setBusy(false);
		notify();
	}

	public Set<Employee> getEmployees() {
		return employees;
	}

	public void addEmployee(Employee employee) {
		this.employees.add(employee);
	}

}
