package com.almundo.callcenter.service.impl;

import java.util.concurrent.Executor;

import com.almundo.callcenter.domain.model.Call;
import com.almundo.callcenter.domain.model.Employee;
import com.almundo.callcenter.service.CallCenterService;
import com.almundo.callcenter.service.Dispatcher;
import com.almundo.callcenter.utils.Constant;

/**
 * Implementacion que despacha la llamada a traves de la ejecucion de un hilo de
 * proceso para la llamada recibida
 * 
 * @author lrincon
 * 
 */
public class DispatcherImpl implements Dispatcher {

	private CallCenterService callCenter;
	private Executor executor;

	public void dispatcherCall(final Call callNew) {
		getExecutor().execute(new Runnable() {

			public void run() {
				dispatch(callNew);
			}
		});
	}

	private void dispatch(final Call callNew) {
		Employee employee = getCallCenter().nextFree();
		if (employee != null) {
			getCallCenter().converse(callNew, employee);
		} else {
			tryCall(callNew);
		}
	}

	private void tryCall(final Call callNew) {
		synchronized (getCallCenter()) {
			try {
				getCallCenter().wait(
						Call.CALL_MAX * Constant.SECONDS_TO_MILLISECONDS);
			} catch (InterruptedException e) {
			}
		}
		dispatch(callNew);
	}

	public CallCenterService getCallCenter() {
		return callCenter;
	}

	public void setCallCenter(CallCenterService callCenter) {
		this.callCenter = callCenter;
	}

	public Executor getExecutor() {
		return executor;
	}

	public void setExecutor(Executor executor) {
		this.executor = executor;
	}

}
