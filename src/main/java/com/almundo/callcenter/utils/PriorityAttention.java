package com.almundo.callcenter.utils;

public enum PriorityAttention {

	ALTA, MEDIA, BAJA;

	public int getLevel() {
		return ordinal();
	}

}
