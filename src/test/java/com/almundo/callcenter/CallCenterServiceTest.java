package com.almundo.callcenter;

import junit.framework.TestCase;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.almundo.callcenter.domain.model.Director;
import com.almundo.callcenter.domain.model.Employee;
import com.almundo.callcenter.domain.model.Operator;
import com.almundo.callcenter.domain.model.Supervisor;
import com.almundo.callcenter.service.impl.CallCenterServiceImpl;
import com.almundo.callcenter.utils.PriorityAttention;

@RunWith(MockitoJUnitRunner.class)
public class CallCenterServiceTest extends TestCase {

	@InjectMocks
	private CallCenterServiceImpl callCenter;

	@org.junit.Test
	public void testNextFreeOperador() {
		Employee operator = prepareEmployee(false, PriorityAttention.ALTA);
		callCenter.addEmployee(operator);
		Employee supervisor = prepareEmployee(false, PriorityAttention.MEDIA);
		callCenter.addEmployee(supervisor);
		Employee director = prepareEmployee(false, PriorityAttention.BAJA);
		callCenter.addEmployee(director);

		Employee result = callCenter.nextFree();

		assertNotNull(result);
		assertEquals(result, operator);
		assertEquals(operator.isBusy(), true);
		assertEquals(supervisor.isBusy(), false);
		assertEquals(director.isBusy(), false);
	}

	@org.junit.Test
	public void testNextFreeSupervisor() {
		Employee operator = prepareEmployee(true, PriorityAttention.ALTA);
		callCenter.addEmployee(operator);
		Employee supervisor = prepareEmployee(false, PriorityAttention.MEDIA);
		callCenter.addEmployee(supervisor);
		Employee director = prepareEmployee(false, PriorityAttention.BAJA);
		callCenter.addEmployee(director);

		Employee result = callCenter.nextFree();

		assertNotNull(result);
		assertEquals(result, supervisor);
		assertEquals(operator.isBusy(), true);
		assertEquals(supervisor.isBusy(), true);
		assertEquals(director.isBusy(), false);
	}

	@org.junit.Test
	public void testNextFreeDirector() {
		Employee operator = prepareEmployee(true, PriorityAttention.ALTA);
		callCenter.addEmployee(operator);
		Employee supervisor = prepareEmployee(true, PriorityAttention.MEDIA);
		callCenter.addEmployee(supervisor);
		Employee director = prepareEmployee(false, PriorityAttention.BAJA);
		callCenter.addEmployee(director);

		Employee result = callCenter.nextFree();

		assertNotNull(result);
		assertEquals(result, director);
		assertEquals(operator.isBusy(), true);
		assertEquals(supervisor.isBusy(), true);
		assertEquals(director.isBusy(), true);
	}

	@org.junit.Test
	public void testNextFreeNone() throws InterruptedException {
		Employee operator = prepareEmployee(true, PriorityAttention.ALTA);
		callCenter.addEmployee(operator);
		Employee supervisor = prepareEmployee(true, PriorityAttention.MEDIA);
		callCenter.addEmployee(supervisor);
		Employee director = prepareEmployee(true, PriorityAttention.BAJA);
		callCenter.addEmployee(director);

		Employee result = callCenter.nextFree();

		assertNull(result);
		assertEquals(operator.isBusy(), true);
		assertEquals(supervisor.isBusy(), true);
		assertEquals(director.isBusy(), true);
	}

	private Employee prepareEmployee(boolean busy, PriorityAttention priority) {
		Employee employee = null;
		switch (priority) {
		case ALTA:
			employee = new Operator();
			break;
		case MEDIA:
			employee = new Supervisor();
			break;
		case BAJA:
			employee = new Director();
			break;
		default:
			break;
		}
		employee.setBusy(busy);
		return employee;
	}

}
