package com.almundo.callcenter;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Executors;

import com.almundo.callcenter.domain.model.AbstractEmployee;
import com.almundo.callcenter.domain.model.Call;
import com.almundo.callcenter.domain.model.Director;
import com.almundo.callcenter.domain.model.Employee;
import com.almundo.callcenter.domain.model.Operator;
import com.almundo.callcenter.domain.model.Supervisor;
import com.almundo.callcenter.service.impl.CallCenterServiceImpl;
import com.almundo.callcenter.service.impl.DispatcherImpl;

public class DispatcherMain {

	private static final int EXTRA_NUMBER_CALLS = 10;

	private static final int NUMBER_DIRECTORS = 1;
	private static final int NUMBER_SUPERVISORS = 2;
	private static final int NUMBER_OPERATORS = 2;

	private DispatcherImpl dispatcher;

	private Set<Employee> prepareEmployees() throws InstantiationException,
			IllegalAccessException, ClassNotFoundException {
		Set<Employee> employees = new TreeSet<Employee>();
		employees.addAll(prepareEmployee(Director.class, NUMBER_DIRECTORS));
		employees.addAll(prepareEmployee(Supervisor.class, NUMBER_SUPERVISORS));
		employees.addAll(prepareEmployee(Operator.class, NUMBER_OPERATORS));
		return employees;
	}

	private Collection<? extends Employee> prepareEmployee(
			Class<? extends Employee> classType, int number)
			throws InstantiationException, IllegalAccessException,
			ClassNotFoundException {
		Set<Employee> employees = new TreeSet<Employee>();
		for (int idx = 0; idx < number; idx++) {
			AbstractEmployee e = (AbstractEmployee) Class.forName(
					classType.getCanonicalName()).newInstance();
			e.setName(classType.getSimpleName() + "-" + idx);
			employees.add(e);
		}
		return employees;
	}

	public static void main(String[] args) throws Exception {
		DispatcherMain run = new DispatcherMain();
		run.run();
	}

	private void run() throws InstantiationException, IllegalAccessException,
			ClassNotFoundException {
		Set<Employee> employees = prepareEmployees();
		CallCenterServiceImpl callCenter = new CallCenterServiceImpl();
		for (Employee e : employees) {
			callCenter.addEmployee(e);
		}
		dispatcher = new DispatcherImpl();
		dispatcher.setCallCenter(callCenter);
		dispatcher.setExecutor(Executors.newCachedThreadPool());
		int numberCalls = employees.size() + EXTRA_NUMBER_CALLS;
		for (int idx = 0; idx < numberCalls; idx++) {
			Call call = new Call();
			call.setNumber(idx);
			dispatcher.dispatcherCall(call);
		}
	}
}
