package com.almundo.callcenter;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import junit.framework.TestCase;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.almundo.callcenter.domain.model.AbstractEmployee;
import com.almundo.callcenter.domain.model.Call;
import com.almundo.callcenter.domain.model.Director;
import com.almundo.callcenter.domain.model.Employee;
import com.almundo.callcenter.domain.model.Operator;
import com.almundo.callcenter.domain.model.Supervisor;
import com.almundo.callcenter.service.impl.CallCenterServiceImpl;
import com.almundo.callcenter.service.impl.DispatcherImpl;

@RunWith(MockitoJUnitRunner.class)
public class DispatcherTest extends TestCase {

	private static final int NUMBER_CALLS = 10;

	private static final int NUMBER_DIRECTORS = 1;
	private static final int NUMBER_SUPERVISORS = 2;
	private static final int NUMBER_OPERATORS = 2;

	@Mock
	private CallCenterServiceImpl callCenter;

	@Mock
	private Executor executor;

	@InjectMocks
	private DispatcherImpl dispatcher;

	@org.junit.Test
	public void testApp() throws Exception {
		setupExecutor();
		Employee employee = mock(Employee.class);
		when(callCenter.nextFree()).thenReturn(employee);

		int numberCalls = NUMBER_CALLS;
		for (int idx = 0; idx < numberCalls; idx++) {
			Call call = new Call();
			call.setNumber(idx);
			dispatcher.dispatcherCall(call);
		}

		verify(callCenter, atLeast(numberCalls)).nextFree();
		verify(callCenter, times(numberCalls)).converse(any(Call.class),
				any(Employee.class));
	}

	public void setupExecutor() {
		Mockito.doAnswer(new Answer<Object>() {
			public Object answer(InvocationOnMock invocation) throws Exception {
				((Runnable) invocation.getArguments()[0]).run();
				return null;
			}
		}).when(executor).execute(any(Runnable.class));
	}

	private Set<Employee> prepareEmployees() throws InstantiationException,
			IllegalAccessException, ClassNotFoundException {
		Set<Employee> employees = new TreeSet<Employee>();
		employees.addAll(prepareEmployee(Director.class, NUMBER_DIRECTORS));
		employees.addAll(prepareEmployee(Supervisor.class, NUMBER_SUPERVISORS));
		employees.addAll(prepareEmployee(Operator.class, NUMBER_OPERATORS));
		return employees;
	}

	private Collection<? extends Employee> prepareEmployee(
			Class<? extends Employee> classType, int number)
			throws InstantiationException, IllegalAccessException,
			ClassNotFoundException {
		Set<Employee> employees = new TreeSet<Employee>();
		for (int idx = 0; idx < number; idx++) {
			AbstractEmployee e = (AbstractEmployee) Class.forName(
					classType.getCanonicalName()).newInstance();
			e.setName(classType.getSimpleName() + "-" + idx);
			employees.add(e);
		}
		return employees;
	}

	public static void main(String[] args) throws Exception {
		DispatcherTest test = new DispatcherTest();
		Set<Employee> employees = test.prepareEmployees();
		CallCenterServiceImpl callCenter = new CallCenterServiceImpl();
		for (Employee e : employees) {
			callCenter.addEmployee(e);
		}
		DispatcherImpl dispatcher = new DispatcherImpl();
		dispatcher.setCallCenter(callCenter);
		dispatcher.setExecutor(Executors.newCachedThreadPool());
		int numberCalls = employees.size() + NUMBER_CALLS;
		for (int idx = 0; idx < numberCalls; idx++) {
			Call call = new Call();
			call.setNumber(idx);
			dispatcher.dispatcherCall(call);
		}
		// test.testApp();
	}
}
